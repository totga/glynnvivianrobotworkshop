import os, os.path
import random
import string
import cherrypy
class StringGenerator(object):
   @cherrypy.expose
   def index(self):
	   return open('index.html')


class RoboLED(object):
	exposed = True

	def __init__(self,  LEDidx):
		self.LEDidx = LEDidx
	@cherrypy.tools.accept(media='text/plain')	
	def POST(self):	
		print("LED idx:" + str(self.LEDidx))
		return ""


class RoboLeft(object):
	exposed = True
	@cherrypy.tools.accept(media='text/plain')	
	def POST(self, speed = 1.0):	
		print("LEEEFT!" + str(speed))
		return ""

class RoboRight(object):
	exposed = True
	@cherrypy.tools.accept(media='text/plain')	
	def POST(self, speed = 1.0):	
		print("RIIIGHT!" + str(speed))
		return ""

class RoboForward(object):
	exposed = True
	@cherrypy.tools.accept(media='text/plain')	
	def POST(self, speed = 1.0):	
		print("FOOORRRWARD!" + str(speed))
		return ""
	
class StringGeneratorWebService(object):
	exposed = True
	@cherrypy.tools.accept(media='text/plain')
	def GET(self):
		return cherrypy.session['mystring']
	def POST(self, length=8):
		some_string = ''.join(random.sample(string.hexdigits, int(length)))
		cherrypy.session['mystring'] = some_string
		return some_string
	def PUT(self, another_string):
		cherrypy.session['mystring'] = another_string
	def DELETE(self):
		cherrypy.session.pop('mystring', None)




if __name__ == '__main__':
	conf = {
		'/': {
			'tools.sessions.on': True,
			'tools.staticdir.root': os.path.abspath(os.getcwd())
		},
		'/generator': {
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/plain')],
		},
		'/roboleft': {
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/plain')],
		},
		'/roboright': {
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/plain')],
		},
		'/roboforward': {
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/plain')],
		},
		'/roboredLED': {
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/plain')],
		},
		'/robogreenLED': {
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/plain')],
		},
		'/roboblueLED': {
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/plain')],
		},
		'/static': {
			'tools.staticdir.on': True,
			'tools.staticdir.dir': './public'
		}
	}
	print("PATH: " + os.path.abspath(os.getcwd()))
	webapp = StringGenerator()
	webapp.generator = StringGeneratorWebService()
	webapp.roboleft = RoboLeft()
	webapp.roboright = RoboRight()
	webapp.roboforward = RoboForward()
	webapp.roboredLED = RoboLED(0)
	webapp.robogreenLED = RoboLED(1)
	webapp.roboblueLED = RoboLED(2)
	cherrypy.quickstart(webapp, '/', conf)
