
import sys, os
from html import escape


import cgi
import cgitb
import cherrypy
import ws4py
import random
import string
import os


PORT = 8000

class HelloWorld(object):
	def index(self):
		return "Hello World!"
	index.exposed = True

def MakeButton(text, cssclass = "gradbox"):
	return "<div class='" + cssclass + "'>" + text + "</div>"


def MakeTextInput(text, valuename, value, action):
	text =  "<form method=\"get\" action=\"" + action + "\"/>\n" +\
			"\t<input type=\"text\" value =\""   + str(value) + "\" name=\"" + str(valuename) +"\" />\n"+\
			"\t<button type=\"submit\">" + text + "</button>\n"+\
			"</form>\n"	
	return text

def MakeDiv(text, divclass= ""):
	return "<div class=\"" + divclass + "\">" + text + "</div>"
		
def MakeDirListingWalk(path):
	text = "<ul>\n"
	for dirname, dirnames, filenames in os.walk(path):		
		for subdirname in dirnames:
			text += "<li> -= "
			text += subdirname 
			text += " =-</li>\n" 
		for filename in filenames:
			text += "<li>" + filename + "</li>\n" 
	text += "</ul>\n"
	return text

def MakeDirListing(path):
	text = "<ul>\n"
	filenames = os.listdir(path)
	text+=MakeDiv("Directories")	
	for filename in filenames:
		fullpath = os.path.join(path, filename)
		if os.path.isdir(fullpath) is True:
			text += "<li> <a href=\"?dirpath=" + fullpath + "\"/>" + filename + "</a></li>\n" 
	text+=MakeDiv("Files")
	for filename in filenames:
		fullpath = os.path.join(path, filename)
		if os.path.isdir(fullpath) is False:		
			text += "<li> <a href=\"showfile?dirpath=" + fullpath + "\"/>" + filename + "</a></li>\n" 

	text += "</ul>\n"
	return text


class StringGeneratorWebService(object):
	exposed = True
	@cherrypy.tools.accept(media='text/plain')
	def GET(self):
		return cherrypy.session['mystring']

	def POST(self, length=8):
		some_string = ''.join(random.sample(string.hexdigits, int(length)))
		cherrypy.session['mystring'] = some_string
		return some_string

	def PUT(self, another_string):
		cherrypy.session['mystring'] = another_string
	def DELETE(self):
		cherrypy.session.pop('mystring', None)


class StringGenerator(object):

	def __init__(self):
		self.header = """<html><head><link href="style.css" rel="stylesheet"></head><body>"""
		self.footer = "</body></html>"
		self.br = "<br/>"
	@cherrypy.expose
	def index(self, dirpath = "\\"):		
		cherrypy.log("hello there")
		return self.header +\
			   MakeTextInput("Enter string", "length", "4", "generate") +\
			   self.br +\
			   MakeButton("Left", "gradbox crvboxtl") + MakeButton("Right", "gradbox crvboxtr") +\
			   self.footer 
			   # MakeDirListing(dirpath) +\
	
	@cherrypy.expose
	def showfile(self, dirpath = "."):
		return self.header + MakeDiv("You chose to view" + dirpath) + self.footer

	@cherrypy.expose
	def generate(self, length=8):
		some_string = ''.join(random.sample(string.hexdigits, int(length)))
		cherrypy.session['mystring'] = some_string
		return some_string

	@cherrypy.expose
	def display(self):
		return cherrypy.session['mystring']

"""
if __name__ == '__main__':
	conf = {
		'/': {
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.sessions.on': True,
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/plain')],
		}
	}
	cherrypy.quickstart(StringGeneratorWebService(), '/', conf)

"""
if __name__ == '__main__':

	cherrypy.config.update(
		{'server.socket_host': '192.168.0.3' ,
		'server.socket_port': 80,

		})
	conf = {
		 '/': {
			'tools.sessions.on': True,						
			'tools.staticdir.on': True,
			'tools.staticdir.root': os.path.abspath(os.getcwd())
		 }
	}
	cherrypy.quickstart(StringGenerator(), '/', conf)
