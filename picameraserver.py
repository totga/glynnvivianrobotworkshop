import io
import socket
import struct
from PIL import Image
import OpenGL
import pyglet
import os
import PIL.Image, PIL.ImageSequence
import time

#IP = "0.0.0.0"
IP = "192.168.0.3"
PORT = 8000
connection = None
server_socket = None

def blit_image(image, pos):
	"""pyglet.gl.glLoadIdentity();
	halfw = image.width * 0.5
	halfh = image.height * 0.5
	pyglet.gl.glTranslatef(halfw,halfh,0);
	pyglet.gl.glScalef(self.scale[0], self.scale[1], 1.0);
	pyglet.gl.glRotatef(self.rotation, 0,0,1);
	pyglet.gl.glTranslatef(-halfw,-halfh,0);		
	pyglet.gl.glTranslatef(pos[0],pos[1], 0)
	blit((0,0))"""
	image.blit(pos[0], pos[1])
	pass

def grab_image(connection):
	try:
		image_len = struct.unpack('<L', connection.read(struct.calcsize('<L')))[0]
		if not image_len:
			return None
		# Construct a stream to hold the image data and read the image
		# data from the connection
		image_stream = io.BytesIO()
		image_stream.write(connection.read(image_len))
		# Rewind the stream, open it as an image with PIL and do some
		# processing on it
		image_stream.seek(0)
		image = Image.open(image_stream)		
		image.verify()
		image.save('test.jpg')

		print('Image is %dx%d' % image.size)
		return image
	except Exception as e:
		print(str(e))
		return None



window = pyglet.window.Window(1024, 768, resizable = True)
counter = 0.0
imgPos = (0,0)
mouseDown = False
selectedLayer = -1

@window.event
def on_mouse_release(x, y, button, modifiers):
	global mouseDown
	print("Mouse up")
	mouseDown = False



def update_frames(dt):
	global counter
	counter = (counter+dt) % 2


@window.event
def on_window_close(window):
	global connection
	global server_socket
	#window.exit()
	print("Closing connection")
	connection.close()
	server_socket.close()
	return pyglet.event.EVENT_HANDLED
	

v = 180
@window.event
def on_draw():
	global connection
	global v
	global imgPos
	#print(counter)
	try :

		image = grab_image(connection)
		#time.sleep(0.1)
		if image != None:
			blit_image(image, (0,0))
			print("Blitting image")
	except Exception as e:
		print("Failed to capture image " + str(e))

	
	
	#v+=1.0;
	
   # frames[int(counter)].blit(320, 200, 0,
	#                          frames[int(counter)].width,
	 #                         frames[int(counter)].height)

#frames = load_anim()

pyglet.clock.schedule_interval(update_frames, 1/30.0)
print("Starting server on " + IP + ":" + str(PORT))
# Start a socket listening for connections on 0.0.0.0:8000 (0.0.0.0 means
# all interfaces)
server_socket = socket.socket()
server_socket.bind((IP, PORT))
server_socket.listen(0)

# Accept a single connection and make a file-like object out of it
connection = server_socket.accept()[0].makefile('rb')

pyglet.app.run()
#connection.close()
#server_socket.close()