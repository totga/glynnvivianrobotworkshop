import socket
import time
import picamera
import iptools

IP = iptools.get_local_IP() #socket.getfqdn(socket.gethostname())#socket.gethostbyname()
PORT = 8000


def CaptureEncodedVideo(timeout):

    with picamera.PiCamera() as camera:
        camera.resolution = (320, 240)
        camera.framerate = 24
    
    
        print("Starting server on " + IP + ":" + str(PORT))
        # Start a socket listening for connections on 0.0.0.0:8000 (0.0.0.0 means
        # all interfaces)
        server_socket = socket.socket()
        server_socket.bind((IP, PORT))
        server_socket.listen(0)
    
        #camera.start_preview()
        time.sleep(2)
        # Accept a single connection and make a file-like object out of it
        connection = server_socket.accept()[0].makefile('wb')
    
        try:
            camera.start_recording(connection, format='h264')
    
            camera.wait_recording(timeout)
            camera.stop_recording()
        finally:
            connection.close()
            server_socket.close()



CaptureEncodedVideo(1000)