
	import os, os.path
import sys
import random
import string
import cherrypy
import pi2go
import time
import iptools
import subprocess
import time
import json
import threading
import glob
import signal
#import psutil

#these write files to a ramdisk, these are picked up by the client app
semaphore_basedir = "/dev/shm/pibot/"
colourfile_fullpath = os.path.join(semaphore_basedir, "colour.json")
distancefile_fullpath = os.path.join(semaphore_basedir, "distance.json")
motorstate_fullpath = os.path.join(semaphore_basedir, "motorstate.json")


LEDon = 4095
LEDoff = 0

BYTE_TO_NFLOAT = 0.00392156862

pi2go.init()

class RoboState(object):
	def __init__(self):
		self.LEDcolours = [{0, 0, 0} for i in range(4)]
		self.left_motor_speed = 0
		self.right_motor_speed= 0
		self.speed = 100.0
		self.turnspeed = 70.0
		self.last_distance = 0
		self.distance = 0
		self.distance_time = 0
		self.last_distance_time = 0
		self.angle_to_speed_ration = 1.0  # calibrate this

	def saveMotorStateFile(self):
		f = open(motorstate_fullpath, "wb")
		f.write(json.dumps([self.left_motor_speed, self.right_motor_speed]))
		f.close()

	def saveDistanceStateFile(self):
		f = open(distancefile_fullpath, "wb")
		f.write(json.dumps([self.distance, self.distance_time]))
		f.close()

	def saveColourState(self):
		f = open(colourfile_fullpath, "wb")
		f.write(json.dumps(self.LEDcolours))
		f.close()

	def move(self, left_speed, right_speed):
		self.left_motor_speed = left_speed
		self.right_motor_speed = right_speed
		pi2go.go(left_speed, right_speed)
		self.saveMotorStateFile()

	def stop(self):
		self.left_motor_speed = 0
		self.right_motor_speed = 0
		pi2go.stop()
		self.saveMotorStateFile()

	def spin_left(self):
		self.move(-self.turnspeed, self.turnspeed)

	def spin_right(self):
		self.move(self.turnspeed, -self.turnspeed)		

	def forward(self):
		self.move(self.speed, self.speed)

	def reverse(self):
		self.move(-self.speed, -self.speed)

	def setLED(self, mask, colour):
		"""Colour channels in range 0..1, converted to 12bit pi range"""
		print("Setting colour: " + str(colour))
		for i in range(4):
			if 1 << i & mask != 0:
				self.LEDcolours[i] = colour
				pi2go.setLED(i, int(colour[0] * 8), int(colour[1] * 8), int(colour[2] * 8))
		#self.saveColourState()

	def get_distance(self):
		"""returns distance in cm"""
		self.last_distance = self.distance
		self.last_distance_time = self.distance_time
		self.distance_time = time.time()
		self.distance = pi2go.getDistance()
		#self.saveDistanceStateFile()
		return self.distance

	def set_speed(self, speed):
		self.speed = speed

	def get_light_sensor(self, idx):
		return pi2go.getLight(idx)


robot = RoboState()

vsn = pi2go.version()
print("Pi2Go Version is " + str(vsn))
try:
	if vsn == 0:
		print("This program only runs on the full Pi2Go")
	else:
		robot.setLED(15, (0, 0, 0))		
except Exception as e:
	print("Problem initialising Pi2Go" + str(e))


def ping_distance_sensor():
	robot.get_distance()
	sleep(100)


def ensureSharedMemoryFiles():
	try:
		if not os.path.exists(semaphore_basedir):
			os.makedirs(semaphore_basedir)
		
	except Exception as e:
		print("Failed to create semaphore base dir")




class RoboLED(object):	
	exposed = True
	@cherrypy.tools.accept(media='text/plain')	
	def POST(self, mask, red, green, blue):
		mask = int(mask)
		red = int(red) 
		green = int(green) 
		blue = int(blue) 
		#print("RobotLED data. Mask: " + str(mask) + ", RGB: (" +str(red) +", " + str(green) + ", " + str(blue)  + ")")
		robot.setLED(mask, (red, green, blue))
		return ""



class RoboPingColourState(object):

	exposed = True
	@cherrypy.tools.accept(media='text/plain')	
	def GET(self):	
		#
		return json.dumps(robot.LEDcolours)		


class GetScriptList(object):

	exposed = True
	@cherrypy.tools.accept(media='text/plain')	
	def GET(self):	
		#
		files = glob.glob("scripts/*.py")
		return json.dumps([os.path.split(f)[1] for f in files])		

class ScriptInfo(object):
	def __init__(self):
		self.script = None
		self.pid = 0
		self.output = None
		self.error = None
		self.cmd = ""

global_script = ScriptInfo()

def start_script(scriptname):
	global global_script
	print("Running script: " +scriptname)

	try:
		global_script.cmd = scriptname
		global_script.script = subprocess.Popen("exec python scripts/" + scriptname, stdout=subprocess.PIPE, shell=True)
	except Exception as e:
		print("Exception occured running script:" + str(e))
	try:

		print("Created sub process:" + str(global_script.script))
		global_script.pid = global_script.script.pid
		print("Script pid = " + str(global_script.pid ))
		global_script.output, global_script.error = global_script.script.communicate() 
	except Exception as e:
		print("Problem occured communicating with script: " + str(e))
	

	print(global_script.output)
	print(global_script.error)
	#result = out.split('\n')
	#global_script = 

def stop_script():

	global global_script
	print("In 'stop_script'")
	if global_script.pid != 0:
		
		try:
			print("stopping script " + global_script.cmd + " with pid " + str(global_script.pid))
		#	process = psutil.Process(global_script.pid)
		#	for proc in global_script.script.get_children(recursive=True):
		#		proc.kill()
			os.kill(global_script.pid, signal.SIGTERM)
			global_script.script.kill()
			
		except Exception as e:
			print("Problem killing subprocess: " + str(e))
		global_script.pid = 0
	else:
		print("Script PID was 0 so can't stop it.")

class RunScript(object):
	exposed = True

	@cherrypy.tools.accept(media='text/plain')	
	def POST(self, scriptname):	
		stop_script()  # stop before start
		start_script(scriptname)
		return ""  

class StopScript(object):
	exposed = True
	@cherrypy.tools.accept(media='text/plain')	
	def POST(self):			
		stop_script()
		return ""  

class RoboPingSensors(object):

	exposed = True
	@cherrypy.tools.accept(media='text/plain')	
	def GET(self):			
		return json.dumps([pi2go.getDistance(), pi2go.getLight(0), pi2go.getLight(1), pi2go.getLight(2), pi2go.getLight(3), pi2go.irLeft(), pi2go.irRight(), pi2go.irCentre(), pi2go.irLeftLine(), pi2go.irRightLine()])		


class RoboSetSpeed(object):

	exposed = True
	@cherrypy.tools.accept(media='text/plain')	
	def POST(self, inspeed):	
		print("Setting speed: " + str(inspeed))
		robot.set_speed(float(inspeed))
		return ""  

class RoboLEDOff(object):
	exposed = True
	@cherrypy.tools.accept(media='text/plain')	
	def POST(self):			
		print("All LEDs being turned off")
		robot.setLED(15, (0,0,0))		
		return ""


class RoboMove(object):
	exposed = True
	@cherrypy.tools.accept(media='text/plain')	
	def POST(self, leftSpeed, rightSpeed):
		robot.move(leftSpeed, rightSpeed)
		return ""
  
class RoboLeft(object):
	exposed = True
	@cherrypy.tools.accept(media='text/plain')	
	def POST(self):	
		robot.spin_left()		
		return ""

class RoboRight(object):
	exposed = True
	@cherrypy.tools.accept(media='text/plain')	
	def POST(self):	
		robot.spin_right()		
		return ""

class RoboForward(object):
	exposed = True
	@cherrypy.tools.accept(media='text/plain')	
	def POST(self):	
		robot.forward()
		return ""

class RoboReverse(object):
	exposed = True
	@cherrypy.tools.accept(media='text/plain')	
	def POST(self):	
		robot.reverse()
		return ""

class RoboStop(object):
	exposed = True
	@cherrypy.tools.accept(media='text/plain')	
	def POST(self):	
		pi2go.stop()
		return ""


class StringGenerator(object):
	@cherrypy.expose
	def index(self):
	   return open('index.html')

	
class StringGeneratorWebService(object):
	exposed = True
	@cherrypy.tools.accept(media='text/plain')
	def GET(self):
		return cherrypy.session['mystring']
	def POST(self, length=8):
		some_string = ''.join(random.sample(string.hexdigits, int(length)))
		cherrypy.session['mystring'] = some_string
		return some_string
	def PUT(self, another_string):
		cherrypy.session['mystring'] = another_string
	def DELETE(self):
		cherrypy.session.pop('mystring', None)


HOSTSTR = iptools.get_local_IP()
PORT = 88

if __name__ == '__main__':
	ensureSharedMemoryFiles()
	#subprocess.call(["../raspimjpeg/raspimjpeg", "--config", "/home/pi/prog/raspimjpeg/raspimjpeg.config", "&"])
	#sensor_ping_thread = threading.Thread(target=ping_distance_sensor)
	#sensor_ping_thread.start()
	argc = len(sys.argv)
	if argc > 1:
		HOSTSTR = sys.argv[1]
		HOSTSTR,portstr = HOSTSTR.split(':')
		if portstr != '':
			PORT = int(portstr)
	
	#TODO: get reliable local server IP here
	cherrypy.config.update(
		{'server.socket_host': HOSTSTR ,
		'server.socket_port': PORT,
		})
	conf = {
		'/': {
			'tools.sessions.on': True,
			'tools.staticdir.root': os.path.abspath(os.getcwd())
		},
		'/generator': {
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/plain')],
		},
		'/roboleft': {
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/plain')],
		},
		'/roboright': {
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/plain')],
		},
		'/roboforward': {
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/plain')],
		},
		'/roboreverse': {
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/plain')],
		},
		'/robostop': {
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/plain')],
		},
		'/roboLED': {
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/plain')],
		},
		'/roboLEDOff': {
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/plain')],
		},
		'/roboSetSpeed': {
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/plain')],
		},
		'/serverwww': {
			'tools.staticdir.on': True,
			'tools.staticdir.dir': '/var/www',
			#'tools.staticdir.debug': True,
		},
		'/smem': {
			'tools.staticdir.on': True,
			'tools.staticdir.dir': '/dev/shm',
			#'tools.staticdir.debug': True,
		},
		'/static': {
			'tools.staticdir.on': True,
			'tools.staticdir.dir': './public',
			#'tools.staticdir.debug': True,
		},
		'/colourstate':{
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/json')],
		},
		'/sensors':{
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/json')],
		},
		'/scriptlist':{
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/json')],
		},
		'/runscript':{
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/plain')],
		},
		'/stopscript':{
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.response_headers.on': True,
			'tools.response_headers.headers': [('Content-Type', 'text/plain')],
		},
		'/worksheet': {
			'tools.staticdir.on': True,
			'tools.staticdir.dir': './public/worksheet',
			#'tools.staticdir.debug': True,
		},

	}
	print("PATH: " + os.path.abspath(os.getcwd()))
	webapp = StringGenerator()
	webapp.generator = StringGeneratorWebService()
	webapp.roboleft = RoboLeft()
	webapp.roboright = RoboRight()
	webapp.roboforward = RoboForward()
	webapp.roboreverse = RoboReverse()
	webapp.robostop = RoboStop()
	webapp.roboLED = RoboLED()
	webapp.roboLEDOff = RoboLEDOff()
	webapp.roboSetSpeed = RoboSetSpeed()
	webapp.sensors = RoboPingSensors()
	webapp.colourstate = RoboPingColourState()
	webapp.scriptlist = GetScriptList()
	webapp.runscript = RunScript()
	webapp.stopscript = StopScript()
	cherrypy.log.error_log.propagate = False
	cherrypy.log.access_log.propagate = False
	cherrypy.log.screen = None

	cherrypy.quickstart(webapp, '/', conf)
	#sensor_ping_thread.stop()
	subprocess.call(["sudo", "killall", "raspimjpeg"])
