import pi2go
import time

print("Running timeset.py")
exit=False
pi2go.init()
total = 0
max_count = 50
counter = 0
realdistance = 0.0
WARNING_LIMIT = 40
print("about to enter main loop")

pi2go.go(100,100)
while exit == False:
	distance = pi2go.getDistance()
	realdistance += distance
	counter = counter + 1
	if counter > max_count:
		
		realdistance = realdistance / max_count
		counter = 0
		print("Average distance : " + str(realdistance))
		if realdistance < WARNING_LIMIT:
			print("Too close!")
			pi2go.go(0,0)
			exit = True
			reldistance = 0.0
			print("Ending")

	time.sleep(0.001)
pi2go.stop()

