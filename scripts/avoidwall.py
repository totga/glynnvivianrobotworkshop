import pi2go
import math
print("RUNNING SCRIPT!")
pi2go.init()
dx = 0
while True:
	dist = pi2go.getDistance()
	if dist > 5.0 and pi2go.irAll() == False:
		#print("No obstacles, forward march!")
		pi2go.forward(100)
	elif pi2go.irLeft():
		print("Left obstacle!")
		pi2go.spinRight(40)
	elif pi2go.irRight():
		print("Right obstacle!")
		pi2go.spinLeft(40)
	elif pi2go.irCentre():
		pi2go.reverse(50)
	else:
		pi2go.stop()
	
print("ENDING SCRIPT!")