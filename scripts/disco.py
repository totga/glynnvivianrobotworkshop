import pi2go
import math
print("RUNNING SCRIPT!")
pi2go.init()
dx = 0
for i in range(1000):
	
	for i in range(4):
		s = i * 0.5
		sx = math.sin(dx + s )
		cx = math.cos(dx + s )
		cx2 = math.cos(dx * 2.0 + s )
		pi2go.setLED(i, int(sx * 4095.0),int(cx * 4095.0),int(cx2 * 4095.0))
	dx += 0.1
print("ENDING SCRIPT!")