import pi2go
import math
import time
print("RUNNING SCRIPT!")
pi2go.init()
dx = 0
ROTATE_SPEED_MULTIPLIER = 5
MOVE_SPEED_MULTIPLIER = 10
MOVE_SPEED_BASE = 50
ROTATE_DIFFERENCE = 10
THRESHOLD = 10
blockedcount = 0
print("Got here")
while True:
	try:
		light = [pi2go.getLightFL(),pi2go.getLightFR(),	pi2go.getLightBL(), pi2go.getLightBR()]
	except Exception as e:
		print("Problem getting light sensor data: " + str(e))
	try:
		front_total = light[0] + light[1]
		rear_total = light[2] + light[3]
		
		#print(light)
		if pi2go.irAll():
			blockedcount += 1
		else:
			blockedcount = 0

		if blockedcount > 10:
			pi2go.reverse(100)
		if (front_total > rear_total) or pi2go.irAll():
			if (light[0] > THRESHOLD or light[1] > THRESHOLD):
	
				diff = light[1] - light[0]
				speed = light[1] + light[0]
				pi2go.go(max(0, min(100, (speed* ROTATE_SPEED_MULTIPLIER) - diff)), max(0, min(100, (speed* ROTATE_SPEED_MULTIPLIER )) + diff ))
				
				"""if diff > ROTATE_DIFFERENCE:
					print("Difference: " +str(diff))
					if light[0] > light[1]:
						pi2go.spinRight(min(100.0, diff * ROTATE_SPEED_MULTIPLIER))
					else:	
						pi2go.spinLeft(min(100.0, diff * ROTATE_SPEED_MULTIPLIER))
				else:
					if pi2go.irAll() == False:
						pi2go.forward(min(100.0, MOVE_SPEED_BASE))"""
			else:
				pi2go.stop()
		else:
			pi2go.spinLeft(min(100.0, 30 * ROTATE_SPEED_MULTIPLIER))
	except Exception as e:
		print("Error occured: " + str(e))
	time.sleep(0.01)
	
print("ENDING SCRIPT!")