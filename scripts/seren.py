import pi2go
import time
import math
exit = False
pi2go.init()
total = 0
max_count = 10
counter = 0
real_distance = 0.0
WARNING_LIMIT = 20
pi2go.go(100,20)

starttime = time.time()
while exit == False:
	ctime = time.time() - starttime
	pi2go.go(math.cos(ctime * 3) * 80, math.sin(ctime * 2) * 80)
	distance = pi2go.getDistance()
	
	print("Distance:" + str(distance) + " sinval: " + str(ctime)) 
	real_distance += distance
	counter = counter + 1
	if counter > max_count:
		real_distance = real_distance / max_count
		print("Average distance" + str(real_distance))
		counter + 0 
		if real_distance < WARNING_LIMIT:
			print("too close! !")
			pi2go.go(0,0)
			exit = True
		real_distance = 0.0
		counter = 0
	
	time.sleep(0.001)
pi2go.go(0,0)
pi2go.stop()

